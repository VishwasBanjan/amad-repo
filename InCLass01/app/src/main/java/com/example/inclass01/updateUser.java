package com.example.inclass01;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static androidx.constraintlayout.widget.Constraints.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link updateUser.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link updateUser#newInstance} factory method to
 * create an instance of this fragment.
 */
public class updateUser extends Fragment {
    EditText firstName,lastName,city,gender;
    ImageButton profileImage;
    Bitmap bitmap;
    FirebaseStorage storage = FirebaseStorage.getInstance("gs://amad-inclass01.appspot.com");
int pressed=0;
    Button update;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseAuth mAuth;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public updateUser() {
        // Required empty public constructor
    }
    public void choosePhotoFromGallary() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto , 1);
    }
    private void takePhotoFromCamera() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, 0);//zero can be replaced with any action code
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null) {
                Uri contentURI = data.getData();
                Log.d("demo12", String.valueOf(contentURI));
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getView().getContext().getContentResolver(), contentURI);
                    //String path =
                    bitmap= Bitmap.createScaledBitmap(bitmap,600,600,true);
                    saveImage(bitmap);
                    //Log.d("demo12",path);
                    Toast.makeText(getView().getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    profileImage.setImageBitmap(bitmap);
                    pressed=1;


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getView().getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == 0) {
            //Log.d("demo12", data.getData().getPath());
            bitmap  = (Bitmap) data.getExtras().get("data");
            bitmap = Bitmap.createScaledBitmap(bitmap,600,600,true);
            profileImage.setImageBitmap(bitmap);
       pressed=1;

            // String path =
             saveImage(bitmap);
            // Log.d("demo12",path);

            Toast.makeText(getView().getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveImage(Bitmap bitmap) {

        mAuth = FirebaseAuth.getInstance();
        final StorageReference storageRef = storage.getReference(  mParam1);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = storageRef.putBytes(data);

        Task<Uri> uriTask=uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                return storageRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                Uri downloadUrii = task.getResult();
                String downloadUri=downloadUrii.toString();



            }
        });
    }






    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment updateUser.
     */
    // TODO: Rename and change types and number of parameters
    public static updateUser newInstance(String param1, String param2) {
        updateUser fragment = new updateUser();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString("email");
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_user, container, false);

        firstName=view.findViewById(R.id.firstName);

        lastName=view.findViewById(R.id.lastName);
        city=view.findViewById(R.id.city);
        gender=view.findViewById(R.id.gender);
        profileImage=view.findViewById(R.id.dp);
        update=view.findViewById(R.id.button);


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (firstName.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getContext(), "Enter First Name", Toast.LENGTH_SHORT).show();
                    firstName.setError("This field can not be blank");
                } else if (lastName.getText().toString().trim().equalsIgnoreCase("")) {
                    Toast.makeText(getContext(), "Enter Last Name", Toast.LENGTH_SHORT).show();
                    lastName.setError("This field can not be blank");
                } else if ((gender.getText().toString().trim().equalsIgnoreCase(""))) {
                    Toast.makeText(getContext(), "Enter gender", Toast.LENGTH_SHORT).show();

                }else if ((city.getText().toString().trim().equalsIgnoreCase(""))) {
                    Toast.makeText(getContext(), "Enter City", Toast.LENGTH_SHORT).show();

                } else if(pressed==0) {
                    DocumentReference washingtonRef = db.collection("users").document(mParam1);

// Set the "isCapital" field of the city 'DC'
                    washingtonRef
                            .update(
                                    "first",firstName.getText().toString(),
                                    "last",lastName.getText().toString(),
                                    "gender",gender.getText().toString(),
                                    "city",city.getText().toString()

                            )

                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "DocumentSnapshot successfully updated!");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error updating document", e);
                                }
                            });

                    Intent intent = new Intent(view.getContext(), MainActivity.class);
                   // intent.setFlag(Intent.CLEAR_TASK);
                    startActivity(intent);



                }

            }
        });

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(takePicture, 0);//zero can be replaced with any action code
                AlertDialog.Builder pictureDialog = new AlertDialog.Builder(view.getContext());
                pictureDialog.setTitle("Select Action");
                String[] pictureDialogItems = {
                        "Select photo from gallery",
                        "Capture photo from camera" };
                pictureDialog.setItems(pictureDialogItems,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        choosePhotoFromGallary();
                                        break;
                                    case 1:
                                        takePhotoFromCamera();
                                        break;
                                }
                            }
                        });
                pictureDialog.show();

            }
        });


        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {

if(String.valueOf(document.getData().get("email")).contentEquals(mParam1)){

                                    firstName.setText(document.getString("first"));
                                    lastName.setText(String.valueOf(document.getData().get("last")));
                                    city.setText(String.valueOf(document.getData().get("city")));
                                    gender.setText(String.valueOf(document.getData().get("gender")));
                                    Picasso.get()
                                            .load(String.valueOf(document.getData().get("image")))
                                            .resize(500, 500)
                                            .centerCrop()
                                            .into(profileImage);
                                    Log.d(TAG, document.getId() + " => " + document.getData());
                                }}

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
