package com.example.inclass01;



public class UserDetails {

    String download;
    String first;
    String last;
    String email;
    String gender;
    String city;
    Boolean isOnline;

    public UserDetails(String download, String first, String last, String email, String gender, String city, Boolean isOnline) {
        this.download = download;
        this.first = first;
        this.last = last;
        this.email = email;
        this.gender = gender;
        this.city = city;
        this.isOnline = isOnline;
    }
}
